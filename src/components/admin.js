import React, { Component } from 'react'
import AddNewBook from "./addNewBook"
import BooksTable from "./booksTable"
import AddNewUser from "./addNewUser"
import UsersTable from "./userTable"
export default class Admin extends Component {


    constructor(){
        super();
        this.state = {
            book:null,
            booksList:[],
            user:null
        }
    }

    add_book(book){
        console.log("admin add book")
        console.log(book)
        this.setState({book: book},()=>{
            console.log(this.state)
        })
    }

    add_user(user){
        console.log("admin add user")
        console.log(user)
        this.setState({user: user},()=>{
            console.log(this.state)
        })
    }


    render() {
        return (
            <div>
                <AddNewBook add_book={(book) => (this.add_book(book))} login_state={this.props.login_state}/>
                <br/>
                <BooksTable login_state={this.props.login_state} new_book={this.state.book} logout={this.props.logout} />
                <br/>
                <AddNewUser login_state={this.props.login_state} add_user={(user)=>(this.add_user(user))} />
                <br/>
                <UsersTable login_state={this.props.login_state} />
            </div>
        )
    }
}
