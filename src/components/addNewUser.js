import React, { Component } from 'react'
const moment = require("moment")

const axios = require("axios")
const base_url = require("../config").base_api_url
const timestamp_format = require("../config").timestamp_format
export default class AddNewUser extends Component {

    constructor(){
        super()
        this.state = {
            name:null,
            username:null,
            password:null,
            membership_days:null,
            reading_hours:null,
            creation_time:null
        }
    }

    handleChange(e){
            this.setState({[e.target.name]:e.target.value},()=>{
                console.log(this.state)
            })
    }

    add_user(e){
        e.preventDefault()
        console.log("/addUser")
        var props = this.props
        console.log(props.login_state)
        this.state.creation_time = moment().format(timestamp_format)
        if (props.login_state){
            var headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.post(base_url + "/users",
                this.state,
                {"headers":headers} 
                ).then(res => {
                console.log("added new user")
                console.log(res.data)
                this.props.add_user(this.state)
            }).catch(err=>{
                console.log("failed to add user")
                console.log(err)
            })
        }
    }

    render() {
        return (
            <div style={{backgroundColor:`rgb(210,210,210)`}}>
                <label>
                    Add New User:
                </label><br/> 
                <form onSubmit={(event)=>(this.add_user(event))}>                           
                    <label>
                        Name:
                        <input type="text" name="name" value={this.state.name} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                    <label>
                        Username:
                        <input type="text" name="username" value={this.state.username} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                    <label>
                        Password:
                        <input type="text" name="password" value={this.state.password} onChange={(event)=>{this.handleChange(event)}} />
                    </label>                                                                
                    <label>
                        Membership_days:
                        <input type="text" name="membership_days" checked={this.state.membership_days} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                    <label>
                        Reading_hours:
                        <input type="text" name="reading_hours" checked={this.state.reading_hours} onChange={(event)=>{this.handleChange(event)}} />
                    </label>                    
                <input type="submit" value="Add/Update User"/> 
                </form>
            </div>
        )
    }
}
