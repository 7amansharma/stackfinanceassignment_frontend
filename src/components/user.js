import React, { Component } from 'react'
import BooksTable from "./booksTable"
import HistoryTable from "./historyTable"
import ChangePassword from "./changePassword"

export default class User extends Component {

    constructor(){
        super();
        this.state = {
            user: {
                name:null,
                username:null,
                password:null,
                membership_days:null,
                reading_hours:null
            }
        }
    }

    render() {
        return (
            <div >
                <ChangePassword login_state={this.props.login_state} logout={this.props.logout} />
                <br/>
                <BooksTable login_state={this.props.login_state} logout={this.props.logout} />
                <br/>
                <HistoryTable login_state={this.props.login_state} logout={this.props.logout}/>
            </div>
        )
    }
}
