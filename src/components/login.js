import React, { Component } from 'react'
import Header from "../components/header"
import Admin from "../components/admin"
import User from "../components/user"
import Watch from "../components/watch"
const moment = require("moment")
const base_url = require("../config").base_api_url

class Login extends Component {

    constructor(){
        console.log("constructor")
        super();
        this.state = {
            login:false,
            username:"",
            password:"",
            role:"user",
            token:null,
            login_message : null,
            current_time: moment().format("YYYY-MM-DD hh:mm:ss")
        }

    }

    update_ui_time(){
        setTimeout(()=>{
            this.setState({current_time: moment().local().format("YYYY-MM-DD HH:mm:ss:Z")},this.update_ui_time())
        },1000);
    }

    handleChange(objToChange,key, event) {
        console.log(objToChange)
        if (objToChange==="state"){
            var obj = {}
            obj[key] = event.target.value;
            console.log(obj)
            this.setState(obj,()=>{
                console.log(this.state)

                if (key==="books_list_condition"){
                    //this.getBooksList()
                }
                
            })
        }else if (objToChange === "book"){
            if (event.target.type === "checkbox"){
                this.book[key] = event.target.checked;
            }else{
                this.book[key] = event.target.value;
            }
            console.log(this.book)
        }else if(objToChange==="user"){
            this.user[key] = event.target.value;
            console.log(this.user)
        }

        //this.newState[key]= event.target.value;
        
    }

    componentDidMount(){
        console.log("didmount")
        this.restoreState()
    }

    restoreState(){
        let store = localStorage.getItem("login")
        console.log("store :"+store)
        if (store){
            console.log("h1")
            store = JSON.parse(store)
            if (store && store.login){
                console.log(store.username)
                console.log(this.setState({username:store.username,role:store.role,token:store.token, login:true},()=>{
                    if (this.state.login){
                        //this.getBooksList()
                        console.log(this.state.books_list)
                    }            
                    console.log("restored state: "+ JSON.stringify(this.state))
                } ))

            }
        }
    }

    fetch_json_response(http_method, api_path, json_string, onSuccess, onFailure){
        console.log(api_path)
        console.log(json_string)
        fetch(base_url+api_path,{
            method:http_method,
            headers:{
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.state.token 
            },
            body:json_string
        }).then((result)=> {
            console.log(api_path + " resolved")
            var response = result.json()
            console.log(response);
            response.then((data)=>{
                console.log(data)
                if (data.success===true){
                    onSuccess(data)
                }else{
                    onFailure(data)
                }
            }).catch(err =>{
                console.log("1: "+ api_path)
                console.log(err)
            }) 
        }).catch(err => {
            console.log("2: "+ api_path)
            console.log(err)
        })
    }


    login() {
        console.log("login")
        console.log(JSON.stringify(this.state))
        fetch(base_url+'/login',{
            method:"POST",
            headers: { "Content-Type": "application/json" },
            body:JSON.stringify(this.state)
        }).then((result)=> {
            console.log("resolved")
            var response = result.json()
            console.log(response);
            response.then((data)=>{
                console.log(data)
                if (data.success===true){
                    this.setState({login:true,token:data.token},()=>{
                        console.log(this.state)
                        localStorage.setItem("login",JSON.stringify({
                            login:true,
                            token:data.token,
                            role:this.state.role,
                            username:this.state.username
                        }))
                        //this.getBooksList();
                    })
                }else{
                    this.setState({login_message:"Incorrect Username or Password"},()=>{
                        console.log("updated login message")
                        console.log(this.state)
                    })
                }
            }).catch(err =>{
                console.log(err)
            }) 
        }).catch(err => {
            console.log(err)
        })

    }



    logout(){
        localStorage.clear();
        console.log(this)
        this.setState({
            login:false,
            username:"",
            password:"",
            role:"user",
            token:null,
            books_list:null,
            books_list_condition:null
        })
    }

    is_token_expired(err){
        console.log("is_token_expired")
        console.log(err)
        if ("name" in err){
            console.log("k1")
            if ("TokenExpiredError"===err["name"]){
                console.log("k2")
                this.logout();
            }

        }
        if ("err" in err){
            console.log("k3")
            if ("name" in err["err"]){
                console.log("k4")
                if ("TokenExpiredError"===err["err"]["name"]){
                    console.log("k5")
                    this.logout();
                }
            }
        }
    }

    render() {
        return (
            <div>
            { !this.state.login?
                <div>
                    {this.state.login_message?<div style={{backgroundColor:"grey",color:"red"}} >{this.state.login_message}</div>:null}

                        <label>
                            Role:
                            <select value={this.state.role} onChange={(event)=>{this.handleChange("state","role", event)}}>
                                <option value="user" defaultValue >User</option>
                                <option value="admin">Admin</option>
                            </select>                        
                        </label>
                        <br/>
                        <label>
                            Username:
                            <input type="text" value={this.state.username} onChange={(event)=>{this.handleChange("state","username", event)}} />
                        </label>
                        <br/>
                        <label>
                            Password:
                            <input type="text" value={this.state.password} onChange={(event)=>{this.handleChange("state","password", event)}} />
                        </label>
                        <br />                                        
                            <button onClick={()=>{this.login()}} >Login</button>
                </div>
                :
                <div>
                    <div>
                        <Header state={this.state} logout={()=>(this.logout()) }/>
                        <Watch/>
                    </div>                    
                    {this.state.role==="admin"? 
                    <Admin login_state={this.state} logout={()=>(this.logout()) } />
                    :(this.state.role==="user"?
                    <User login_state={this.state} logout={()=>(this.logout()) }/>
                    :null)
                    }
                </div>
            }
            </div>
        )
    }
}

export default Login
