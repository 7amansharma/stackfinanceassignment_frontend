import React, { Component } from 'react'
import moment from 'moment';
const axios = require("axios") 

const base_url = require("../config").base_api_url
const timestamp_format = require("../config").timestamp_format

export default class BooksTable extends Component {

    constructor(){
        super();
        this.state = {
            books_list : [],
            filter_condition:"all"
        }
    }

    handleChange(e){
        this.setState({[e.target.name]:e.target.value},()=>{
            console.log(this.state)
            this.getBooksList(this.props)
        })
    }

    getBooksList(props){
        console.log(">>>>getBooksList")
        console.log(props)
        if (props.login_state){
            const headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.get(base_url + "/books",{
                params: {"condition":"all","timestamp":moment().format(timestamp_format)},
                headers 
            }).then(res => {
                console.log("new booklist")
                console.log(res.data)
                if (!res.data.booksList){
                    if (res.data.code){
                        if (res.data.code==="jwtToken"){
                            this.props.logout();
                        }
                    }
                }
                var books_list = JSON.parse(res.data.booksList)
                books_list = books_list.filter((book)=>{
                    book["issueReqType"] = book["issueReqType"]==1?"One Day":(book["issueReqType"]==7?"Seven Days":book["issueReqType"]) 
                    if (this.state.filter_condition==="available"){
                        return book.status==="available";
                    }else if (this.state.filter_condition==="issued"){
                        return book.status === "issued"
                    }else if (this.state.filter_condition==="issuedToMe"){
                        return book.status==="issued" && this.props.login_state.username===book.issuedTo
                    }else{
                        return true
                    }
                })
                this.setState({books_list:books_list },()=>{
                    console.log("updated state of table")
                    console.log(this.state)
                })
            }).catch(err=>{
                console.log("failed to get books")
                console.log(err)
            })
        }
    }


    delete_book(isbn){
        console.log("delete book")
        var props = this.props
        console.log(props)
        if (props.login_state){
            const headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.delete(base_url + "/books",{
                params: {"isbn":isbn},
                headers 
            }).then(res => {
                console.log("deleting book")
                console.log(res.data)
                if (res.data.success){
                    this.setState({books_list:this.state.books_list.filter((book)=>{return book.isbn!==isbn})},(()=>{
                        console.log(this.state)
                    }))
                }
            }).catch(err=>{
                console.log("failed to delete books")
                console.log(err)
            })
        }
    }

    send_issue_request(book, request_type){
        console.log("issue request")
        console.log(book)
        var props = this.props 
        if (props.login_state){
            var headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.post(base_url + "/issueRequest",
                {"book":book,"type":request_type,"timestamp":moment().format(timestamp_format)},
                {"headers":headers} 
                ).then(res => {
                console.log("requested book for issuing ")
                console.log(res.data)
                if(res.data.success){
                    this.setState({books_list:this.state.books_list.map((book_item)=>{
                        if (book.isbn===book_item.isbn){
                            book_item["issueReqFrom"] = this.props.login_state.username
                            book_item["issueReqType"] = request_type===1?"One Day":"Seven Days"
                            book_item["pastReturnTime"] = "N/A"
                        }
                        return book_item
                    })},()=>{
                        console.log(this.state)
                    })
                }else{
                    console.log("1 :failed to request book for issue")
                    console.log(res.data)
                    if (res.data.err){
                        alert(res.data.err)
                    }
                }
                //this.props.add_book(book)
            }).catch(err=>{
                console.log("failed to request book for issue")
                console.log(err)
            })
        }
    }

    handle_issue_request(book, action){
        console.log("issue request")
        console.log(book)
        book.issueReqType = book.issueReqType=="One Day"?1:(book.issueReqType=="Seven Days"?7:book.issueReqType)
        var props = this.props 
        console.log(props)
        if (props.login_state){
            var request_time = moment().format(timestamp_format);
            var headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.post(base_url + "/handleIssueRequest",
                {"book":book,"action":action,"timestamp":request_time},
                {"headers":headers} 
                ).then(res => {
                console.log("handling issue request ")
                console.log(res.data)
                if (res.data.success){

                    var temp_books_list =  this.state.books_list.map((book_item)=>{
                        if (book.isbn===book_item.isbn && action === "accept"){
                            book_item.issuedTo = book.issueReqFrom
                            book_item.status = "issued"
                            book_item.issueReqFrom = "N/A" 
                            book_item.issuedTime = request_time
                            book_item.pastReturnTime = "No"
                            book_item.issueReqType = book_item.issueReqType==1?"One Day":(book_item.issueReqType==7?"Seven Days":book_item.issueReqType) 

                        }else if (book.isbn===book_item.isbn && action === "decline"){
                            book_item.issuedTo = "N/A"
                            book_item.status = "available"
                            book_item.issueReqFrom = "N/A"
                            book_item.issueReqType = "N/A"
                            book_item.pastReturnTime = "N/A"                      
                        }
                        return book_item
                    })
                    this.setState({books_list:temp_books_list},()=>{
                        console.log(this.state)
                    })
                }else{
                    console.log("failed to handle issue request")
                    console.log(res.data)
                }
                //this.props.add_book(book)
            }).catch(err=>{
                console.log("failed to handle issue request")
                console.log(err)
            })
        }
    }


    acceptBook(book){
        console.log("Accept book")
        console.log(book)
        console.log(this.state)
        var props = this.props 
        if (props.login_state){
            var headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.post(base_url + "/acceptBook",
                book,
                {"headers":headers} 
                ).then(res => {
                console.log("accept issued book back")
                console.log(res.data)
                if (res.data.success){
                    var temp_array = []
                    this.state.books_list.forEach((book_item)=>{
                        var objCopy = Object.assign({}, book_item);
                        if (book.isbn===objCopy.isbn ){
                            objCopy.issuedTo = "N/A"
                            objCopy.status = "available"
                            objCopy.issueReqFrom = "N/A"
                            objCopy.issueReqType = "N/A"
                            objCopy.issuedTime = "N/A"
                            objCopy.pastReturnTime = "N/A"
                        }
                        temp_array.push(objCopy)
                    })
                    this.setState({books_list:temp_array},()=>{
                        console.log("heeeeeeeer")
                        console.log(this.state)
                    })
                }else{
                    console.log("failed to accept  request")
                    console.log(res.data)
                }
                //this.props.add_book(book)
            }).catch(err=>{
                console.log("failed to accept  request")
                console.log(err)
            })
        }
    }




    componentDidMount(){
        this.getBooksList(this.props)
    }

    componentWillReceiveProps(){
        this.getBooksList(this.props)
    }

    render() {
        return (
            <div>
                {this.props.login_state?
                <div>
                <label>
                    Books List:
                </label>
                <label>
                    <select name="filter_condition" value={this.state.filter_condition} onChange={(event)=>{this.handleChange(event)}}>
                        <option value="all" defaultValue >All</option>
                        <option value="issued">Issued</option>
                        <option value="available">Available</option>
                        {this.props.login_state.role==="user"?<option value="issuedToMe">IssuedToMe</option>:null}
                    </select>                        
                </label></div>:null
                }                            
                { this.state.books_list.length > 0?<div>
                        <table>
                        <thead >
                            <tr  >
                                {Object.keys(this.state.books_list[0]).map((key)=>(
                                    <th key={key}  style={{border: "1px solid black"}}> {key!=="_id"?key:null}</th>
                                    ))}
                            </tr>
                        </thead>
                        <tbody>
                            { this.state.books_list.map((book)=>(
                                <tr  key={book["isbn"]} style={{backgroundColor: book.pastReturnTime=="Yes"?'red':'white'}} >
                                    { Object.keys(this.state.books_list[0]).map((col)=>(
                                        <td style={{border: "1px solid black"}} key={book["isbn"]+col} >{col!=="_id"?(col==="canBeIssued"? (book[col]===true?<label>Yes</label>:<label>No</label>) :book[col]):null }</td>
                                    ))}
                                    {(book["status"]==="available" && this.props.login_state.role==="admin")?
                                    <td key={book["isbn"]+"delete"}>
                                        <button onClick={()=>{book["status"]==="issued"?alert("Cannot delete an issued book"):this.delete_book(book["isbn"])}} >Delete</button>
                                    </td>
                                    :null
                                    }
                                    { (book["issueReqFrom"]!=="N/A" && this.props.login_state.role==="admin")?
                                    <td key={book["isbn"]+"accept"}>
                                        <button onClick={()=>{this.handle_issue_request(book,"accept")}} >Accpet issue request</button>
                                    </td>
                                    :null
                                    }                                
                                    { (book["issueReqFrom"]!=="N/A" && this.props.login_state.role==="admin")?
                                    <td key={book["isbn"] + "decline"}>
                                        <button onClick={()=>{this.handle_issue_request(book,"decline")}} >Decline issue request</button>
                                    </td>
                                    :null
                                    }                                                                
                                    { (book["status"]==="available" && this.props.login_state.role==="user")?
                                    <td key={book["isbn"]+"request"+"1"}>
                                        <button onClick={()=>{this.send_issue_request(book,1)}} >Send issue request for today</button>
                                    </td>
                                    :null
                                    }
                                    { (book["status"]==="available" && this.props.login_state.role==="user" && book.canBeIssued===true)?
                                    <td key={book["isbn"]+"request" + "7"}>
                                        <button onClick={()=>{this.send_issue_request(book,7)}} >Send issue request for 7 days</button>
                                    </td>
                                    :null
                                    }
                                    { (book["status"]==="issued" && this.props.login_state.role==="admin")?
                                    <td key={book["isbn"]+"accept"}>
                                        <button onClick={() => this.acceptBook(book) } >Accept Book</button>
                                    </td>
                                    :null
                                    }                                                                                                                                
                                </tr>
                        ))  }
                        </tbody>
                    </table>
                </div>
                :null}                
            </div>
        )
    }
}
