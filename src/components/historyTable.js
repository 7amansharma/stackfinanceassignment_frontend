import React, { Component } from 'react'
import moment from 'moment';
const axios = require("axios") 

const base_url = require("../config").base_api_url
const timestamp_format = require("../config").timestamp_format

export default class historyTable extends Component {

    constructor(){
        super();
        this.state = {
            history_list : []
        }
    }

    handleChange(e){
        this.setState({[e.target.name]:e.target.value},()=>{
            console.log(this.state)
            this.getHistoryList(this.props)
        })
    }

    getHistoryList(props){
        console.log(">>>>getHistoryList")
        console.log(props)
        if (props.login_state){
            const headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.get(base_url + "/history",{
                params: {"condition":"all","timestamp":moment().format(timestamp_format)},
                headers 
            }).then(res => {
                console.log("new historylist")
                console.log(res.data)
                if (!res.data.historyList){
                    if (res.data.code){
                        if (res.data.code==="jwtToken"){
                            this.props.logout();
                        }
                    }
                }
                var history_list = JSON.parse(res.data.historyList)
                this.setState({history_list:history_list },()=>{
                    console.log("updated state of table")
                    console.log(this.state)
                })
            }).catch(err=>{
                console.log("failed to get history")
                console.log(err)
            })
        }
    }


    delete_history(id){
        console.log("delete history")
        var props = this.props
        console.log(props)
        if (props.login_state){
            const headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.delete(base_url + "/history",{
                params: {"id":id},
                headers 
            }).then(res => {
                console.log("deleting history")
                console.log(res.data)
                if (res.data.success){
                    this.setState({history_list:this.state.history_list.filter((history)=>{return history.id!==id})},(()=>{
                        console.log(this.state)
                    }))
                }
            }).catch(err=>{
                console.log("failed to delete history")
                console.log(err)
            })
        }
    }

    componentDidMount(){
        this.getHistoryList(this.props)
    }

    componentWillReceiveProps(){
        this.getHistoryList(this.props)
    }

    render() {
        return (
            <div>
                {this.props.login_state?
                <div>
                <label>
                    History List:{(this.state.history_list.length > 0)?
                                        <button
                                            onClick={()=>{this.delete_history()}} 
                                            >Delete</button>
                                    :null
                                    }
                </label>
                </div>:null
                }                            
                { this.state.history_list.length > 0?<div>
                        <table>
                        <thead >
                            <tr  >
                                    <th style={{border: "1px solid black"}}> Timestamp </th>
                                    <th style={{border: "1px solid black"}}> Event </th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.state.history_list.map((history)=>(
                                <tr  key={history["id"]} style={{backgroundColor: history.pastReturnTime=="Yes"?'red':'white'}} >
                                    
                                        <td style={{border: "1px solid black"}}  >{history["timestamp"]}</td>
                                        <td style={{border: "1px solid black"}}  >{history["event"]}</td>
                                </tr>
                        ))  }
                        </tbody>
                    </table>
                </div>
                :null}                
            </div>
        )
    }
}
