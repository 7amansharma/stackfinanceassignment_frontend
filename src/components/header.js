import React from 'react'

export default function Header(props) {
    return (
        <div>
            <div>
                <h1>
                {props.state.role==="admin"?
                    "Admin Page"
                :
                    "User Page"
                }
                <button onClick={()=>{props.logout()}} style={{marginLeft:20}} >Logout</button>
                </h1>
            </div>
        </div>
    )
}


