import React, { Component } from 'react'
const axios = require("axios")
const base_url = require("../config").base_api_url

export default class AddNewBook extends Component {

    constructor(){
        super()
        this.state =  {
            name:"",
            author:"",
            isbn:"",
            canBeIssued:false
        }
    }

    handleChange(e){
        if (e.target.name==="canBeIssued"){
            this.setState({[e.target.name]:!this.state.canBeIssued},()=>{
                console.log(this.state)
            })
        }else{
            this.setState({[e.target.name]:e.target.value},()=>{
                console.log(this.state)
            })
        }
    }

    add_books(e){
        e.preventDefault()
        console.log("/addBook")
        var props = this.props
        console.log(props.login_state)
        if (props.login_state){
            var headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.post(base_url + "/books",
                this.state,
                {"headers":headers} 
                ).then(res => {
                console.log("add new book")
                console.log(res.data)
                this.props.add_book(this.state)
            }).catch(err=>{
                console.log("failed to add book")
                console.log(err)
            })
        }
    }

    render() {
        return (
            <div style={{backgroundColor:`rgb(210,210,210)`}}>
                <label>
                    Add New Book:
                </label><br/> 
                <form onSubmit={(event)=>(this.add_books(event))}>                           
                    <label>
                        Book Name:
                        <input type="text" name="name" value={this.state.name} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                    <label>
                        Author:
                        <input type="text" name="author" value={this.state.author} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                    <label>
                        ISBN No:
                        <input type="text" name="isbn" value={this.state.isbn} onChange={(event)=>{this.handleChange(event)}} />
                    </label>                                                                
                    <label>
                        Can Be Issued:
                        <input type="checkbox" name="canBeIssued" checked={this.state.canBeIssued} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                <input type="submit" value="Add/Update Book"/> 
                </form>
            </div>
        )
    }
}
