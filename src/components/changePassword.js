import React, { Component } from 'react'
const moment = require("moment")

const axios = require("axios")
const base_url = require("../config").base_api_url
const timestamp_format = require("../config").timestamp_format

export default class ChangePassword extends Component {

    constructor(){
        super();
        this.state = {
            old_password:null,
            new_password:null
        }
    }


    handleChange(e){
        this.setState({[e.target.name]:e.target.value},()=>{
            console.log(this.state)
        })
}

update_password(e){
    e.preventDefault()
    console.log("/changePassword")
    var props = this.props
    console.log(props.login_state)
    this.state.creation_time = moment().format(timestamp_format)
    if (props.login_state){
        var headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + props.login_state.token         
        }
        axios.post(base_url + "/changePassword",
            this.state,
            {"headers":headers} 
            ).then(res => {
            console.log("change password")
            console.log(res.data)
            if(res.data.success){
                alert("Password Change Successful, Please login again with new password")
                this.props.logout()
            }else if (res.data.err==="Incorrect Current Password"){
                alert("Password Change Failed: Incorrect Current Password")
            }else{
                alert("Change Password Failed")
            }
            //this.props.add_user(this.state)
        }).catch(err=>{
            console.log("failed to change password")
            console.log(err)
        })
    }
}

    render() {
        return (
            <div style={{backgroundColor:`rgb(210,210,210)`}}>
                <label>
                    Change Password:
                </label><br/> 
                <form onSubmit={(event)=>(this.update_password(event))}>                           
                    <label>
                        Old Password:
                        <input type="text" name="old_password" value={this.state.old_password} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                    <label>
                        New Password:
                        <input type="text" name="new_password" value={this.state.new_password} onChange={(event)=>{this.handleChange(event)}} />
                    </label>
                <input type="submit" value="Change Password"/> 
                </form>                
            </div>
        )
    }
}
