import React, { Component } from 'react'
const axios = require("axios") 
const moment = require("moment")
const base_url = require("../config").base_api_url
const timestamp_format = require("../config").timestamp_format

export default class UsersTable extends Component {

    constructor(){
        super();
        this.state = {
            users_list : [],
            filter_condition:"all"
        }
    }

    handleChange(e){
        this.setState({[e.target.name]:e.target.value},()=>{
            console.log(this.state)
            this.getUsersList(this.props)
        })
    }

    getUsersList(props){
        console.log(">>>>getUsersList")
        console.log(props)
        var time_stamp = moment().format(timestamp_format)
        if (props.login_state){
            const headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.get(base_url + "/users",{
                params: {"condition":"all","timestamp":time_stamp},
                headers 
            }).then(res => {
                console.log("new userlist")
                console.log(res.data)
                var users_list = JSON.parse(res.data.usersList)
                users_list = users_list.filter((user)=>{
                    if (this.state.filter_condition==="available"){
                        return user.canBeIssued===true && user.status==="available";
                    }else if (this.state.filter_condition==="issued"){
                        return user.status === "issued"
                    }else if (this.state.filter_condition==="issuedToMe"){
                        return user.status==="issued" && this.props.login_state.username===user.issuedTo
                    }else{
                        return true
                    }
                })
                this.setState({users_list:users_list },()=>{
                    console.log("updated state of table")
                    console.log(this.state)
                })
            }).catch(err=>{
                console.log("failed to get users")
                console.log(err)
            })
        }
    }


    delete_user(username){
        console.log("delete user")
        var props = this.props
        console.log(props)
        if (props.login_state){
            const headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + props.login_state.token         
            }
            axios.delete(base_url + "/users",{
                params: {"username":username},
                headers 
            }).then(res => {
                console.log("deleting user")
                console.log(res.data)
                if (res.data.success){
                    this.setState({users_list:this.state.users_list.filter((user)=>{return user.username!==username})},(()=>{
                        console.log(this.state)
                    }))
                }
            }).catch(err=>{
                console.log("failed to delete users")
                console.log(err)
            })
        }
    }



    componentDidMount(){
        this.getUsersList(this.props)
    }

    componentWillReceiveProps(){
        this.getUsersList(this.props)
    }

    render() {
        return (
            <div>
                {this.props.login_state?
                <div>
                <label>
                    Users List:
                </label>
                <label>
                    <select name="filter_condition" value={this.state.filter_condition} onChange={(event)=>{this.handleChange(event)}}>
                        <option value="all" defaultValue >All</option>
                        <option value="hasBooksIssued">HasusersIssued</option>
                    </select>                        
                </label></div>:null
                }                            
                { this.state.users_list.length > 0?<div>
                        <table>
                        <thead >
                            <tr  >
                                {Object.keys(this.state.users_list[0]).map((key)=>(
                                    <th key={key}  style={{border: "1px solid black"}}> {key!=="_id"?key:null}</th>
                                    ))}
                            </tr>
                        </thead>
                        <tbody>
                            { this.state.users_list.map((user)=>(
                                <tr  key={user["username"] } style={{backgroundColor:user["MembershipExpiresInDays"]==0?"red":"white"}}>
                                    { Object.keys(this.state.users_list[0]).map((col)=>(
                                        <td style={{border: "1px solid black"}} key={user["username"]+col} >{col!=="_id"?(user[col]):null }</td>
                                    ))}
                                    {(this.props.login_state.role==="admin")?
                                    <td key={user["username"]+"delete"}>
                                        <button onClick={()=>{user["status"]==="hasBooksIssued"?alert("Cannot delete an user with issued Books"):this.delete_user(user["username"])}} >Delete</button>
                                    </td>
                                    :null
                                    }
                                </tr>
                        ))  }
                        </tbody>
                    </table>
                </div>
                :null}                
            </div>
        )
    }
}
