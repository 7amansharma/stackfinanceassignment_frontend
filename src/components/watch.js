import React, { Component } from 'react'
const moment = require("moment")
const timestamp_format = require("../config").timestamp_format

export default class Watch extends Component {
    constructor(){
        super()
        this.state = {
            current_time : moment().format(timestamp_format)
        }

        this.update_time()
    }

    update_time(){
        this.setState({"current_time":moment().format(timestamp_format)})
        setTimeout(()=>{
            this.update_time()
        },1000)
    }
    render() {
        return (
            <div align="right" style={{paddingRight:10, paddingTop:10}}>
                Current Local Time: {this.state.current_time}                
            </div>
        )
    }
}
